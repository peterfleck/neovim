local keymap = vim.keymap

-- Map <leader>l to insert console.log() in insert mode
keymap.set("n", "<leader>cl", "i console.log()<Esc>i", { noremap = true, silent = true, desc = "Insert console.log()" })

-- NvimTree

keymap.set("n", "<C-n>", "<cmd> NvimTreeToggle <CR>", { desc = "Toggle nvimtree" })
keymap.set("n", "<leader>e", "<cmd> NvimTreeFocus <CR>", { desc = "Focus nvimtree" })

-- Terminal
keymap.set({ "n" }, "<C-t>", ":vsplit term://$SHELL<CR>i", { desc = "Toggle terminal" })

-- Git
keymap.set("n", "<leader>grh", "<cmd>!git reset --hard HEAD<CR>", { desc = "Git reset hard HEAD" })
keymap.set(
	"n",
	"<leader>gl",
	':vsplit term://$SHELL<CR>igit log --oneline --decorate --graph --all --format="%C(auto)%h%C(reset) %C(bold cyan)(%an)%C(reset) %C(bold magenta)%as%C(reset) %C(auto)%d%C(reset) %s" -n 19<CR>',
	{ desc = "Git log oneline" }
)
keymap.set(
	"n",
	"<leader>gbl",
	":vsplit term://$SHELL<CR>igit for-each-ref --sort=-committerdate --format='%(HEAD) %(color:yellow)%(refname:short)%(color:reset) - %(color:red)%(authorname)%(color:reset) %(color:blue)%(objectname:short)%(color:reset) - %(color:green)%(committerdate:relative)%(color:reset)' refs/heads refs/remotes<CR>",
	{ desc = "Git Branches List" }
)

-- Websites
keymap.set(
	"n",
	"<leader>og",
	':execute "!open https://www.google.com/search?q=" . shellescape(@@, 1)<CR><ESC>',
	{ desc = "Open google search" }
)
keymap.set("n", "<leader>ob", ":!open https://getbootstrap.com/<CR><ESC>", { desc = "Open getbootstrap.com" })
keymap.set("n", "<leader>ot", ":!open https://tabler.io/docs/getting-started<CR><ESC>", { desc = "Open tabler docs" })

-- VS Code
keymap.set("n", "<leader>ofc", ":!code %<CR><ESC>", { desc = "Open current file with VS Code" })
keymap.set("n", "<leader>odc", ":!code .<CR><ESC>", { desc = "Open current directory with VS Code" })

-- Preping Greek text
vim.cmd([[
function! PrepGreekText()  
  %s/\d\+//g
  %s/[,;:·]/\0\r/g
  %s/[(]/\r\0/g
  %s/\./\.\r\r/g
  %s/\(\S\)\s\+\(κατὰ \|ἐν \|μετὰ \|ἐκ \|εἰς \|πρὸς \|ὑπὲρ \|ἀλλὰ \|ἐπὶ\|περὶ\|ὡς \|πρός \)/\1\r\2/g
  %s/\(\S\)\s\+\(ἐξ \|διὰ \|διʼ \|μεθ’ \|παρʼ \|παρὰ \|ἀπὸ \|ἵνα \|καθὼς \|ἀλλʼ\|ὅτε \)/\1\r\2/g
  %s/\(\S\)\s\+\(ὑπὸ \|ἐπʼ \|μετʼ \)/\1\r\2/g
  %s/\(\S\)\s\+\(ὅτι \|καὶ \)\zs/\r/g
  %s/^\s\+//g
  %s/\Cἀπὸ /**Ground\/Reason:** || **Locative (Source):** || **Locative (Separation):** ἀπὸ /g
  %s/\Cδιʼ /**Manner (Agency) gen:** || **Manner (Means) gen:** || **Ground\/Reason acc:** || **Locative gen:** || **Temporal gen:** διʼ /g
  %s/\Cδιὰ /**Manner (Agency) gen:** || **Manner (Means) gen:** || **Ground\/Reason acc:** || **Locative gen:** || **Temporal gen:** διὰ /g
  %s/\CΔιὰ /**Manner (Agency) gen:** || **Manner (Means) gen:** || **Ground\/Reason acc:** || **Locative gen:** || **Temporal gen:** Διὰ /g
  %s/\Cἵνα /**Purpose:** || **Result:** || **Explanation:** ἵνα /g
  %s/\Cκαθὼς /**Comparison:** || **Temporal:** καθὼς /g
  %s/\Cκατὰ /**Purpose acc:** || **Explanation (Reference) acc:** || **Standard:** || **Locative gen,acc:** || **Locative (Source):** || **Temporal:** κατὰ /g
  %s/\Cὅτε /**Temporal:** ὅτε /g
  %s/\Cπερὶ /**Purpose (Advantage) gen:** || **Explanation (Reference) gen\/acc:** || **Locative acc:** || **Temporal: acc** περὶ/g
  %s/\Cπρὸ /**Locative:** || **Temporal:** πρὸ /g
  %s/\Cπρὸς /**Purpose (Disadvantage):** || **Result:** || **Explanation (Accompaniment):** || **Locative (Destination):** πρὸς /g
  %s/\Cὑπὲρ /**Purpose (Advantage) gen:** || **Explanation (Reference) gen:** || **Comparison acc:** || **Locative acc:** || **Substitutio**  ὑπὲρ /g
  %s/\Cὑπὸ /**Manner (Agency) gen:** || **Locative acc:** ὑπὸ /g
  %s/\Cὡς /**Purpose:** || **Comparison:** || **Temporal:** ὡς /g
endfunction
]])

vim.api.nvim_set_keymap("n", "<leader>x", ":call PrepGreekText()<CR>", { noremap = true, silent = false })
