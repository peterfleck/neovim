local opt = vim.opt

-- Tab / Indentation
opt.expandtab = true
opt.shiftwidth = 2
opt.smartindent = true
opt.softtabstop = 2
opt.tabstop = 2
opt.wrap = false

-- Search
opt.hlsearch = true
opt.ignorecase = true
opt.incsearch = true
opt.smartcase = true

-- Appearance
opt.cmdheight = 1
opt.colorcolumn = "100"
opt.completeopt = "menuone,noinsert,noselect"
opt.cursorline = true
opt.number = true
opt.relativenumber = true
opt.scrolloff = 10
opt.signcolumn = "yes"
-- opt.statuscolumn = "%l%s%r "
opt.termguicolors = true

-- Behaviour
opt.autochdir = false
opt.backspace = "indent,eol,start"
opt.backup = false
opt.clipboard:append("unnamedplus")
opt.encoding = "UTF-8"
opt.errorbells = false
opt.hidden = true
opt.iskeyword:append("-")
opt.modifiable = true
opt.mouse:append("a")
opt.spelllang = "en_gb"
opt.spell = true
opt.splitbelow = true
opt.splitright = true
opt.swapfile = false
opt.undodir = vim.fn.expand("~/.vim/undodir")
opt.undofile = true

-- Folding
opt.foldenable = true
opt.foldexpr = "nvim_treesitter#foldexpr()"
opt.foldlevel = 99
opt.foldmethod = "expr"
