-- Yanking
vim.cmd("autocmd TextYankPost * lua vim.highlight.on_yank { higroup = 'Search', timeout = 400 }")

-- Terminal
vim.cmd("autocmd TermOpen * setlocal nonumber norelativenumber") -- switch off line numbers in a terminal

-- Macros
-- Put a return after a comma, colon, semicolon or full stop
-- Done in PrepGreekText function. Keeping here for reference of how to load macros
-- vim.cmd([[let @g = "/[,:;.·]\<CR>a\<CR>\<ESC>"]])
