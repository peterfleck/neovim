local env = {}

-- Path to OmniSharp.dll
env.omniSharpPath = "/home/parallels/.local/share/nvim/mason/packages/omnisharp/libexec/OmniSharp.dll"

-- Catppuccin Theme - latte, frappe, macchiato, mocha
env.catppuccinFlavour = "mocha"

return env
