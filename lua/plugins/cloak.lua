return {
	"laytan/cloak.nvim", -- https://github.com/laytan/cloak.nvim
	event = "VeryLazy",
	name = "cloak",
	config = function()
		local cloak = require("cloak")
		cloak.setup({
			cloak_character = "*",
			cloak_length = nil, -- Provide a number if you want to hide the true length of the value.
			enabled = true,
			highlight_group = "Comment",
			try_all_patterns = true,
			patterns = {
				{
					cloak_pattern = "=.+",
					file_pattern = ".env*",
					replace = nil,
				},
			},
		})
	end,
}
