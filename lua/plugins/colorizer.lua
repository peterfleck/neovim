return {
	"norcalli/nvim-colorizer.lua",
	event = { "BufReadPre", "BufNewFile" },
	config = function()
		require("colorizer").setup({
			"css",
			"html",
			"javascript",
			"scss",
			-- html = {
			-- 	mode = "foreground",
			-- },
		})
	end,
}
