local config = function()
	require("nvim-treesitter.configs").setup({
		build = ":TSUpdate",
		indent = {
			enable = true,
		},
		autotag = {
			enable = true,
		},
		event = {
			"BufReadPre",
			"BufNewFile",
		},
		ensure_installed = {
			"bash",
			"c",
			"c_sharp",
			"css",
			"csv",
			"dockerfile",
			"gitignore",
			"graphql",
			"html",
			"http",
			"javascript",
			"jsdoc",
			"json",
			"latex",
			"lua",
			"markdown",
			"mermaid",
			"powershell",
			"python",
			"scss",
			"sql",
			"svelte",
			"terraform",
			"tmux",
			"typescript",
			"vim",
			"vimdoc",
			"xml",
			"yaml",
		},
		auto_install = true,
		highlight = {
			enable = true,
			additional_vim_regex_highlighting = true,
			use_languagetree = true,
		},
		-- LOOK AT THESE AND CHANGE TO SUIT
		incremental_selection = {
			enable = true,
			keymaps = {
				init_selection = "<C-s>",
				node_incremental = "<C-s>",
				scope_incremental = false,
				node_decremental = "<BS>",
			},
		},
	})
end

return {
	-- Highlight, edit, and navigate code
	"nvim-treesitter/nvim-treesitter",
	dependencies = {
		"nvim-treesitter/nvim-treesitter-textobjects",
		"windwp/nvim-ts-autotag",
	},
	lazy = false,
	config = config,
}
