local env = require("config.env")

return {
	"catppuccin/nvim",
	name = "catppuccin",
	priority = 1000,

	config = function()
		-- local latte = require("catppuccin.palettes").get_palette("latte")
		-- local frappe = require("catppuccin.palettes").get_palette("frappe")
		-- local macchiato = require("catppuccin.palettes").get_palette("macchiato")
		-- 	local mocha = require("catppuccin.palettes").get_palette("mocha")
		local catppuccin = require("catppuccin")

		catppuccin.setup({
			flavour = env.catppuccinFlavour, -- latte, frappe, macchiato, mocha
			integrations = {
				cmp = true,
				fidget = true,
				gitsigns = true,
				notify = true,
				nvimtree = true,
				telescope = true,
				treesitter = true,
				which_key = true,
				indent_blankline = {
					enabled = true,
					colored_indent_levels = true,
				},
			},
		})
		vim.cmd("colorscheme catppuccin")
	end,
}
