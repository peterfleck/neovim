return {
	"linrongbin16/gitlinker.nvim",
	dependencies = { { "nvim-lua/plenary.nvim" } },
	event = "VeryLazy",

	config = function()
		local gitlinker = require("gitlinker")

		gitlinker.setup({
			message = false,
			console_log = false,
		})
	end,
}
