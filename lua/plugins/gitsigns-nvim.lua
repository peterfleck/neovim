return {
	"lewis6991/gitsigns.nvim",
	lazy = false,
	config = function()
		require("gitsigns").setup({
			current_line_blame = true,
			current_line_blame_formatter = "<author>, <abbrev_sha> [<author_time:%Y-%m-%d %H:%M:%S>] - <summary>",
			linehl = true,
		})
	end,
}
