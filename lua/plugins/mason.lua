return {
	"williamboman/mason.nvim",
	dependencies = {
		"williamboman/mason-lspconfig.nvim",
		"WhoIsSethDaniel/mason-tool-installer.nvim",
	},
	config = function()
		local mason = require("mason")
		local mason_lspconfig = require("mason-lspconfig")
		local mason_tool_installer = require("mason-tool-installer")

		-- enable mason and configure icons
		mason.setup({
			ui = {
				icons = {
					package_installed = "✓",
					package_pending = "➜",
					package_uninstalled = "✗",
				},
			},
		})

		mason_lspconfig.setup({
			-- list of servers for mason to install
			ensure_installed = {
				"azure_pipelines_ls",
				"bashls",
				"cssls",
				"emmet_ls",
				"emmet_ls",
				"eslint",
				"graphql",
				"html",
				"jsonls",
				"lua_ls",
				"marksman",
				"omnisharp",
				"prismals",
				"pyright",
				"svelte",
				"terraformls",
				"ts_ls",
				"typos_lsp",
				"yamlls",
			},
			-- auto-install configured servers (with lspconfig)
			automatic_installation = true, -- not the same as ensure_installed
		})

		mason_tool_installer.setup({
			ensure_installed = {
				"black", -- python formatter
				"csharpier", -- csharp formatter
				"eslint_d", -- js linter
				"isort", -- python formatter
				"js-debug-adapter", -- JS debugger
				"netcoredbg", -- C# debugger
				"prettier", -- prettier formatter
				"pylint", -- python linter
				"shfmt", -- bash formatter
				"stylua", -- lua formatter
				"tflint", -- terraform linter
			},
		})
	end,
}
